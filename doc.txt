Install python3.3
Install virtualenv
Install rabbitmq and configure it. (http://celery.readthedocs.org/en/latest/getting-started/brokers/rabbitmq.html)
Install apt-get install libxml2-dev libxslt-dev

Clone code
Make new virtualenvironment:
(if there are multiple versions):

virtualenv -p /usr/bin/python3.3 .venv

(if there is 1 version):
virtualenv .venv

Activate new virtualenv:

source .venv/bin/activate

pip install -r requirements.txt

redis
flask
flask-sqlalchemy
celery
lxml